package com.kdmforce.ecommerce.controllers;


import com.kdmforce.ecommerce.model.CustomUserDetails;
import com.kdmforce.ecommerce.model.entity.User;
import com.kdmforce.ecommerce.services.UserServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Created by palla on 19/09/17.
 */
@RestController
@RequestMapping("/api/user")
public class UserConstroller {

    private static Logger logger = LoggerFactory.getLogger(UserConstroller.class);

    @Autowired
    private UserServices userServices;

    @RequestMapping(value ="/get-all", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAll() {

        logger.debug("Get All Users");


        return new ResponseEntity<List<User>>(userServices.getAll(), HttpStatus.OK);


    }

    @RequestMapping(value = "/get-by-id/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getAccount(@PathVariable Long id) {
        logger.debug("Ricerca dell'utente con id " + id);

        return new ResponseEntity<User>(userServices.getOneById(id), HttpStatus.OK);

    }

    @RequestMapping(value = "/get-user-info", method = RequestMethod.GET)
    public ResponseEntity<User> getUserInfo() {
        logger.debug("Ricerca delle informazioni dell'utente attualmente loggato ");
        CustomUserDetails customerUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return new ResponseEntity<User>(userServices.getByUsername(customerUserDetails.getUsername()), HttpStatus.OK);

    }
}
