package com.kdmforce.ecommerce.services;

import com.kdmforce.ecommerce.model.CustomUserDetails;
import com.kdmforce.ecommerce.model.entity.User;
import com.kdmforce.ecommerce.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerUserDetailsServices implements UserDetailsService {
    private static Logger logger = LoggerFactory.getLogger(CustomerUserDetailsServices.class);


    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> userOptional = userRepository.findOneByUsername(username);

        userOptional
                .orElseThrow( () -> new UsernameNotFoundException("User not found!"));

        logger.debug("sto cercando l'utente");
        return userOptional.map(userDTO -> new CustomUserDetails(userDTO))
                .get();
    }

}

