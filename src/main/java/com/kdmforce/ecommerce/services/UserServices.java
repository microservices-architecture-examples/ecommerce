package com.kdmforce.ecommerce.services;

import com.kdmforce.ecommerce.model.entity.User;
import com.kdmforce.ecommerce.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServices {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAll(){
        return userRepository.findAll();
    }

    public User getOneById(Long id){
        return userRepository.findOne(id);
    }

    public void save(User user){
        userRepository.save(user);
    }

    public User getByUsername(String username){
        return userRepository.findOneByUsername(username).get();
    }

}
