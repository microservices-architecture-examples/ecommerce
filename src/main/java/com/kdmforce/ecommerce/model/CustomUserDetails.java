package com.kdmforce.ecommerce.model;

import com.kdmforce.ecommerce.model.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

public class CustomUserDetails extends User implements UserDetails {
    private boolean enable;

    public CustomUserDetails(User user) {
        super(user);

        if (user.getAuthoritiesSet().size() > 0) {
            this.enable = user.isActivated();
        } else {
            this.enable = false;
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      return  getAuthoritiesSet().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName()))
        .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enable;
    }
}
