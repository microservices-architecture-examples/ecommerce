package com.kdmforce.ecommerce.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by palla on 19/09/17.
 */
@Entity
@Table(name = "users")
public class User implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;

    private String password;

    private boolean isActivated;


    @OneToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<CustomUserAuthorities> authoritiesSet;



    public User() {

    }

    public User(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.isActivated = user.isActivated();
        this.authoritiesSet = user.getAuthoritiesSet();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public Set<CustomUserAuthorities> getAuthoritiesSet() {
        return authoritiesSet;
    }

    public void setAuthoritiesSet(Set<CustomUserAuthorities> authoritiesSet) {
        this.authoritiesSet = authoritiesSet;
    }

}
