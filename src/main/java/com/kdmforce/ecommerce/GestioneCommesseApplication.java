package com.kdmforce.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;


@SpringBootApplication
@EnableResourceServer
//@PropertySource("file:///gestione-commesse/config/application.properties")
public class GestioneCommesseApplication extends SpringBootServletInitializer{


    public static void main(String[] args) {
        SpringApplication.run(GestioneCommesseApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(GestioneCommesseApplication.class);
    }


}