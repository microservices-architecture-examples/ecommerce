package com.kdmforce.ecommerce.repositories;


import com.kdmforce.ecommerce.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by palla on 19/09/17.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByUsername(String username);

    User findOne(Long id);
}
